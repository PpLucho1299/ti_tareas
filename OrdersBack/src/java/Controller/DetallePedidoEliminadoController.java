/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.DetallePedidoEliminadoDao;
import Models.DetallePedidoEliminado;
import javax.swing.table.DefaultTableModel;


public class DetallePedidoEliminadoController {

    DetallePedidoEliminadoDao dpedao;
    String message;

    public DetallePedidoEliminadoController() {
        this.message = "";
    }

    public String insertarDetallePedidoEliminado(String id_pedido) {
        dpedao = new DetallePedidoEliminadoDao();
        this.message = "Error en los parametros ingresados";

        if (dpedao.insertarDetallePedidoEliminado(id_pedido)) {
            this.message = "correcto";
        } else {
            this.message = "error en la base de datos";
        }

        return this.message;
    }

    public String aumentarStock(String id_producto, String id_cantidad) {
        dpedao = new DetallePedidoEliminadoDao();
        DetallePedidoEliminado detm = new DetallePedidoEliminado();
        this.message = "Error en los parametros ingresados";

        detm.setId_producto(id_producto);
        detm.setCantidad(id_cantidad);

        if (dpedao.aumentarStock(detm)) {
            this.message = "correcto";
        } else {
            this.message = "error de base de datos";
        }
        return this.message;
    }

    public String getProductos(String id_pedido) {
        dpedao = new DetallePedidoEliminadoDao();
        DefaultTableModel table = dpedao.getProductos(id_pedido);
        String detalle = "";
        for (int i = 0; i < table.getRowCount(); i++) {
            detalle += table.getValueAt(i, 0).toString() + ";" + table.getValueAt(i, 4).toString();
            if (i < table.getRowCount() - 1) {
                detalle += "/";
            }
        }
        System.out.println(detalle);
        return detalle;
    }

}
