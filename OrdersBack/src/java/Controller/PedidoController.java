/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.PedidoDao;
import Models.Pedido;


public class PedidoController {

    PedidoDao pedao;
    String message;

    public PedidoController() {
        this.message = "";
    }

    public String insertarPedido(String estado, String id_usuario) {
        pedao = new PedidoDao();
        Pedido pe = new Pedido();

        this.message = "Error en los parametros de entrada";

        pe.setEstado(estado);
        pe.setId_usuario(id_usuario);

        if (pedao.insertarPedido(pe)) {
            this.message = "procesando...";
        } else {
            this.message = "error de base de datos";
        }
        return this.message;
    }
    
    public String eliminarPedido(String id_pedido) {
        pedao = new PedidoDao();
        Pedido pe = new Pedido();

        this.message = "Error en los parametros de entrada";

        pe.setId_pedido(id_pedido);

        if (pedao.eliminarPedido(pe)) {
            this.message = "pedidoEliminado";
        } else {
            this.message = "error de base de datos";
        }
        return this.message;
    }
    
    

    public String listarPedidos(String id_usuario) {
        pedao = new PedidoDao();
        return pedao.listarPedidos(id_usuario);
    }
    
    public String listarPedidosCancelados(String id_usuario) {
        pedao = new PedidoDao();
        return pedao.listarPedidosCancelados(id_usuario);
    }
    
    
    public String listarPedidosTienda(String id_usuario){
        pedao = new PedidoDao();
        Pedido pe = new Pedido();
        
        pe.setId_usuario(id_usuario);
        return pedao.listarPedidosTienda(pe);
    }
    
    public String cancelarPedido (String id_pedido){
        pedao = new PedidoDao();
        if(pedao.cancelarPedido(id_pedido)){
                this.message = "Pedido cancelado correctamente";
        }else{
            this.message = "error";
        }
        return this.message;
    }
    
    public String despacharPedido (String id_pedido){
        pedao = new PedidoDao();
        if(pedao.despacharPedido(id_pedido)){
                this.message = "Pedido despachado correctamente";
        }else{
            this.message = "error";
        }
        return this.message;
    }
    
}
