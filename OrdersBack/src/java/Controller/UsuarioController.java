/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UsuarioDao;
import DataStatic.Methods;
import Models.UserSession;
import Models.Usuario;
import javax.swing.table.DefaultTableModel;


public class UsuarioController {

    UsuarioDao usdao;
    String message;

    public UsuarioController() {
        message = "";
    }

    public String insertarUsuario(String nombres, String apellidos, String nombre_tienda, String estado, String tipo_usuario, String usuario, String contrasenia) {
        usdao = new UsuarioDao();
        Usuario us = new Usuario();
        this.message = "Error en los parametros de entrada.";
        us.setNombres(nombres);
        us.setApellidos(apellidos);
        us.setNombre_tienda(nombre_tienda);
        us.setEstado(estado);
        us.setTipo_usuario(tipo_usuario);
        us.setUsuario(usuario);
        us.setContrasenia(contrasenia);
        if (usdao.insertarUsuario(us)) {
            this.message = "Usuario registrado correctamente.";
        } else {
            this.message = "Erro al agregar un nuevo usuario.";
        }
        return this.message;
    }

    public String habilitarUsuario(String id_usuario) {
        usdao = new UsuarioDao();
        Usuario us = new Usuario();
        this.message = "Error en los parametros de entrada.";
        us.setId_usuario(id_usuario);
        if (usdao.habilitar(us)) {
            this.message = "Usuario habilitado correctamente";
        } else {
            this.message = "Error al habilitar el usuario";
        }
        return this.message;
    }

    public UserSession login(String usuario, String contrasenia) {
        UserSession usr = null;
        usdao = new UsuarioDao();
        DefaultTableModel employeeresponse = usdao.login(usuario, contrasenia);
        if (employeeresponse.getRowCount() > 0) {
            usr = new UserSession();
            usr.setId_user(employeeresponse.getValueAt(0, 0).toString());
            usr.setNombre(employeeresponse.getValueAt(0, 1).toString());
            usr.setApellido(employeeresponse.getValueAt(0, 2).toString());
            usr.setNombre_usuario(employeeresponse.getValueAt(0, 6).toString());
            usr.setCargo(employeeresponse.getValueAt(0, 5).toString());
            usr.setEstado(employeeresponse.getValueAt(0, 4).toString());
        } else {
            usr = null;
        }
        return usr;
    }

    public String listarTiendas() {
        usdao = new UsuarioDao();
        return usdao.listarTiendas();
    }

    public String listarClientes() {
        usdao = new UsuarioDao();
        return usdao.listarClientes();
    }

    public DefaultTableModel logIngPro(String usuario, String contrasenia) {
        System.out.println("que tirooo :v "+usuario +" "+contrasenia);
        usdao = new UsuarioDao();
        return usdao.login(usuario, contrasenia);
    }

    public String[] LogIn(String usuario, String pwd) {

        DefaultTableModel table = logIngPro(usuario, pwd);
        String message = "User not found.";
        String status = "4";
        Usuario usr = new Usuario();
        if (table.getRowCount() > 0) {
            message = "Incorrect Password.";
            status = "3";
            for (int index = 0; index < table.getRowCount(); index++) {
                if (pwd.equals(table.getValueAt(index, 7).toString())) {
                    usr = usdao.setUser(table, index);
                    message = "Access granted.";
                    status = "2";
                }
            }
        }
        /**
         * The next is is used to verify that the account is confirmated.
         */
        if (usr.getEstado().equals("d")) {
            status = "5";
        }
        System.out.println(message);
        return new String[]{status, message, jsonUser(usr)};
    }

    private String jsonUser(Usuario usr) {
        return usdao.userDataJson(usr);
    }

}
