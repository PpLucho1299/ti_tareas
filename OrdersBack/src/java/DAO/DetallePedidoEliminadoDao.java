/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DataStatic.Conection;
import Models.DetallePedidoEliminado;
import javax.swing.table.DefaultTableModel;


public class DetallePedidoEliminadoDao {
    
    Conection conex;
    String sql = "";
    
    public DetallePedidoEliminadoDao(){
        conex = new Conection();
    }
    
    public boolean insertarDetallePedidoEliminado(String id_pedido){
        sql = "insert into detalle_pedido_eliminado (cantidad, precio_unit, encabezado_pedido_eliminado_id_pedeliminado, producto_id_producto)"
                + "select cantidad, precio_unit, encabezado_pedido_id_encapedido, producto_id_producto from detalle_pedido \n" +
"	   where encabezado_pedido_id_encapedido = "+id_pedido+"";
        System.out.println(sql);
        return conex.modifyBD(sql);
    }
    
    public boolean aumentarStock(DetallePedidoEliminado detp){
        sql = String.format("update producto set stock = stock + %s where id_producto = %s", detp.getCantidad(), detp.getId_producto());
        return conex.modifyBD(sql);
    }
    
    public DefaultTableModel getProductos (String id_pedido){
        sql = "select * from detalle_pedido_eliminado where encabezado_pedido_eliminado_id_pedeliminado = "+id_pedido+"";
        System.out.println(sql);
        return conex.returnRecord(sql);
    }
    
    
}
