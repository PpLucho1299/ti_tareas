/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DataStatic.Conection;
import Models.Pedido;


public class PedidoDao {

    Conection conex;
    String sql = "";

    public PedidoDao() {
        conex = new Conection();
    }

    public boolean insertarPedido(Pedido pd) {
        sql = String.format("insert into encabezado_pedido (fecha_venta, estado, usuario_id_usuario) values (now(),'%s',%s)", pd.getEstado(), pd.getId_usuario());
        return conex.modifyBD(sql);
    }
    
    public boolean eliminarPedido(Pedido pd){
        sql = "delete from encabezado_pedido where id_encapedido = "+pd.getId_pedido()+"";
        return conex.modifyBD(sql);
    }

    public boolean cancelarPedido(Pedido pd) {
        sql = String.format("delte from encabezado_pedido where id_encapedido = %s", pd.getId_pedido());
        return conex.modifyBD(sql);
    }

    public String listarPedidos(String id_usuario) {
        sql = "SELECT * FROM ENCABEZADO_PEDIDO WHERE USUARIO_ID_USUARIO = "+id_usuario+" GROUP BY ID_ENCAPEDIDO ";
        return conex.getRecordsInJson(sql);
    }
    
    public String listarPedidosCancelados (String id_usuario){
        sql = "SELECT * FROM encabezado_pedido_eliminado WHERE USUARIO_ID_USUARIO = "+id_usuario+"";
        return conex.getRecordsInJson(sql);
    }

    public String listarPedidosTienda(Pedido pd) {
        sql = "SELECT ep.id_encapedido, ep.fecha_venta, ep.estado \n"
                + "FROM ENCABEZADO_PEDIDO AS EP INNER JOIN DETALLE_PEDIDO as DP ON EP.ID_ENCAPEDIDO = DP.ENCABEZADO_PEDIDO_ID_ENCAPEDIDO\n"
                + "inner join producto as pd on dp.producto_id_producto = pd.id_producto \n"
                + "inner join usuario as us on pd.usuario_id_usuario = us.id_usuario where us.id_usuario = "+pd.getId_usuario()+" \n"
                + "group by ep.id_encapedido, ep.fecha_venta, ep.estado";
        System.out.println(sql);
        return conex.getRecordsInJson(sql);
    }

    public boolean cancelarPedido(String id_pedido) {
        sql = "update encabezado_pedido set estado = 'c' where id_encapedido = " + id_pedido + "";
        System.out.println(sql);
        return conex.modifyBD(sql);
    }
    
    public boolean despacharPedido(String id_pedido) {
        sql = "update encabezado_pedido set estado = 'd' where id_encapedido = " + id_pedido + "";
        System.out.println(sql);
        return conex.modifyBD(sql);
    }

}
