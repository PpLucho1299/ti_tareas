/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DataStatic.Conection;
import Models.PedidoEliminado;


public class PedidoEliminadoDao {
    
    Conection conex;
    String sql = "";
    
    public PedidoEliminadoDao(){
        conex = new Conection();
    }
    
    public boolean insertarPedidoEliminado(PedidoEliminado pe){
        sql = String.format("insert into encabezado_pedido_eliminado (fecha_eliminar, fecha_pedido, usuario_id_usuario) values(now(), '%s', %s)",pe.getFecha_pedido(), pe.getId_usuario());
        return conex.modifyBD(sql);
    }
}
