/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DataStatic.Conection;
import Models.Usuario;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.swing.table.DefaultTableModel;


public class UsuarioDao {

    Conection conex;
    String sql = "";

    public UsuarioDao() {
        conex = new Conection();
    }

    public boolean insertarUsuario(Usuario us) {
        sql = String.format("insert into usuario (nombres, apellidos, nombre_tienda, estado, tipo_usuario,"
                + "usuario, contrasenia) values('%s','%s','%s','%s','%s','%s','%s')", us.getNombres(),
                us.getApellidos(), us.getNombre_tienda(), us.getEstado(), us.getTipo_usuario(), us.getUsuario(), us.getContrasenia());
        return conex.modifyBD(sql);
    }

    public boolean habilitar(Usuario us) {
        sql = "update usuario set estado = 'a' where id_usuario = " + us.getId_usuario() + "";
        return conex.modifyBD(sql);
    }

    public DefaultTableModel login(String nombre_user, String contrasenia) {
        sql = "select * from usuario where usuario = '" + nombre_user + "' and contrasenia = '" + contrasenia + "'";
        System.out.println(sql);
        return conex.returnRecord(sql);
    }

    public String listarTiendas() {
        sql = "select * from usuario where tipo_usuario = 'tienda'";
        return conex.getRecordsInJson(sql);
    }

    public String listarClientes() {
        sql = "select * from usuario where tipo_usuario = 'cliente'";
        return conex.getRecordsInJson(sql);
    }

    public String userDataJson(Usuario usr) {
        String key = "digiclave";
        long tiempo = System.currentTimeMillis();
        String jwt = Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, key)
                .setSubject("-1")
                .claim("user", usr.getId_usuario())
                .claim("permit", usr.getTipo_usuario())
                .setIssuedAt(new Date(tiempo))
                .setExpiration(new Date(tiempo + 900000))
                .compact();

        JsonObjectBuilder jsoB = Json.createObjectBuilder();
        jsoB.add("lastname_user", usr.getApellidos());
        jsoB.add("names_user", usr.getUsuario());
        jsoB.add("type_user", usr.getTipo_usuario());
        jsoB.add("estado", usr.getEstado());
        jsoB.add("user_token", jwt);
        javax.json.JsonObject jsonObj = jsoB.build();
        return jsonObj.toString();
    }

    public Usuario setUser(DefaultTableModel table, int index) {
        Usuario usr = new Usuario();
        if (-1 < index && index < table.getRowCount()) {
            usr.setId_usuario(table.getValueAt(0, 0).toString());
            usr.setNombres(table.getValueAt(0, 1).toString());
            usr.setApellidos(table.getValueAt(0, 2).toString());
            usr.setNombre_tienda(table.getValueAt(0, 3).toString());
            usr.setEstado(table.getValueAt(0, 4).toString());
            usr.setTipo_usuario(table.getValueAt(0, 5).toString());
            usr.setUsuario(table.getValueAt(0, 6).toString());

        }
        return usr;
    }

}
