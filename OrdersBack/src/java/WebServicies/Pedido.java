/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServicies;

import Controller.DetallePedidoController;
import Controller.DetallePedidoEliminadoController;
import Controller.PedidoController;
import Controller.PedidoEliminadoController;
import DataStatic.Methods;
import com.google.gson.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("pedido")
public class Pedido {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    PedidoController pedcon;

    public Pedido() {
        pedcon = new PedidoController();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarPedidosTienda")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarPedidosTienda(String data) {
        String message;
        System.out.println("listarPedidosTienda()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pedcon.listarPedidosTienda(clains[0]);
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarTiendas")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarTiendas(String data) {
        String message;
        System.out.println("listarTiendas()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pedcon.listarPedidos(clains[0]);
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/aprobarPedido")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response aprobarPedido(String data) {
        String message;
        System.out.println("aprobarPedido()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");

            String id_pedido = Methods.JsonToString(Jso, "id_pedido", "");

            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pedcon.despacharPedido(id_pedido);
                message = "{\"message\":\"" + res + "\"}";
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/guardarPedido")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarPedido(String data) {
        String message;
        System.out.println("guardarPedido()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");

            String detalle = Methods.JsonToString(Jso, "detalle", "");

            System.out.println(detalle);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String estado = "p";
                String res = "";
                String status = pedcon.insertarPedido(estado, clains[0]);
                if (status.equals("procesando...")) {
                    String[] detalle_0 = detalle.split("/");
                    for (int i = 0; i < detalle_0.length; i++) {
                        String[] detalle_1 = detalle_0[i].split(";");
                        DetallePedidoController detpecon = new DetallePedidoController();
                        //detpecon.disminuirStock(detalle_1[0], detalle_1[1]);
                        res = detpecon.insertarDetallePedido(detalle_1[0], detalle_1[1], detalle_1[2]);
                    }
                }
                message = "{\"message\":\"" + res + "\"}";
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/cancelarPedido")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cancelarPedido(String data) {
        String message = "";
        System.out.println("cancelarPedido()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");

            String id_pedido = Methods.JsonToString(Jso, "id_pedido", "");
            String fecha_pedido = Methods.JsonToString(Jso, "fecha_pedido", "");
            
            System.out.println(id_pedido+" "+fecha_pedido);

            PedidoEliminadoController pedelcon = new PedidoEliminadoController();

            System.out.println(id_pedido);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = "";
                String status = pedelcon.insertarPedidoEliminado(fecha_pedido, clains[0]);
                if (status.equals("corecto")) {
                    DetallePedidoEliminadoController detpecon = new DetallePedidoEliminadoController();
                    res = detpecon.insertarDetallePedidoEliminado(id_pedido);
                    System.out.println(res);
                    System.out.println(detpecon.getProductos(id_pedido));
                    String[] detalle_0 = detpecon.getProductos(id_pedido).split("/");
                    for (int i = 0; i < detalle_0.length; i++) {
                        String[] detalle_1 = detalle_0[i].split(";");
                        detpecon.aumentarStock(detalle_1[0], detalle_1[1]);
                    }
                    DetallePedidoController detpeco = new DetallePedidoController();
                    detpeco.eliminardetalle(id_pedido);
                    res = pedcon.eliminarPedido(id_pedido);
                    message = "{\"message\":\"" + res + "\"}";
                    System.out.println(res);
                }
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarPedidosCancelados")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarPedidosCancelados(String data) {
        String message;
        System.out.println("listarPedidosCancelados()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pedcon.listarPedidosCancelados(clains[0]);
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

}
