/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServicies;

import Controller.DetallePedidoController;
import Controller.PedidoController;
import Controller.ProductoController;
import DataStatic.Methods;
import com.google.gson.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("producto")
public class Producto {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    ProductoController pdcon;

    public Producto() {
        pdcon = new ProductoController();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarProductos")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarProductos(String data) {
        String message;
        System.out.println("listarProductos()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pdcon.listarProductos(clains[0]);
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarProductosTienda")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarProductosTienda(String data) {
        String message;
        System.out.println("listarProductosTienda()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            String id_usuario = Methods.JsonToString(Jso, "id_usuario", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pdcon.listarProductosTienda(id_usuario);
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/saveProduct")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveProduct(String data) {
        String message;
        System.out.println("saveProduct()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");

            String nombre = Methods.JsonToString(Jso, "nombre", "");
            String stock = Methods.JsonToString(Jso, "stock", "");
            String precio = Methods.JsonToString(Jso, "precio", "");

            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = pdcon.insertarProducto(nombre, stock, precio, clains[0]);
                message = "{\"message\":\"" + res + "\"}";
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }
    
        @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/disminuiStock")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response disminuiStock(String data) {
        String message;
        System.out.println("disminuiStock()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");

            String id_prodcuto = Methods.JsonToString(Jso, "id_producto", "");
            String cantidad = Methods.JsonToString(Jso, "cantidad", "");

            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                DetallePedidoController detp = new DetallePedidoController();
                String res = detp.disminuirStock(id_prodcuto, cantidad);
                message = "{\"message\":\"" + res + "\"}";
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

}
