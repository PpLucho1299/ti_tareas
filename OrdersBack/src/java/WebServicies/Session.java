/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServicies;

import Controller.UsuarioController;
import DataStatic.Methods;
import com.google.gson.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("session")
public class Session {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;
    
    UsuarioController ucontrol;
    
    public Session (){ ucontrol = new UsuarioController(); }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response LogIn(String data) {
        String message;
        System.out.println("LogIn()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String user = Methods.JsonToString(Jso, "usr", "");
            String pwd = Methods.JsonToString(Jso, "pwd", "");
           System.out.println(user+":"+pwd);
            String[] res = ucontrol.LogIn(user, pwd);
            System.out.println(res[2]);
            message = "{\"status\":" + res[0] + ",\"information\":\"" + res[1] + "\",\"data\":" + res[2] + "}";
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/getdatasession")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getDataSession(String data) {
        JsonObject Jso = Methods.stringToJSON(data);
        String user_token = Methods.JsonToString(Jso, "user_token", "");
        String[] res = Methods.getDataToJwt(user_token);
        String jsonresponse = "{\"status\":" + (res[0].equals("") ? 4 : (res[1].equals("sleep"))?3:2) + ",\"information\":\"" + (res.equals("") ? "Invalid Token." : "All ok.\"") 
                + ",\"data\":{\"permmit\":\""+res[1]+"\"}}";
        return Response.ok(jsonresponse)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

}
