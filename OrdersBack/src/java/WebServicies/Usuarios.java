/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServicies;

import Controller.UsuarioController;
import DataStatic.Methods;
import com.google.gson.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("usuarios")
public class Usuarios {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    UsuarioController ucontrol;

    public Usuarios() {
        ucontrol = new UsuarioController();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/registrerUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registrerUser(String data) {
        String message;
        System.out.println("registrerUser()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {

            String nombres = Methods.JsonToString(Jso, "nombres", "");
            String apellidos = Methods.JsonToString(Jso, "apellidos", "");
            String nombre_tienda = Methods.JsonToString(Jso, "nombre_tienda", "");
            String nombre_user = Methods.JsonToString(Jso, "usuario", "");
            String contrasenia = Methods.JsonToString(Jso, "contrasenia", "");
            String tipo_usuario = Methods.JsonToString(Jso, "tipo_usuario", "");
            String estado = "d";

            String res = ucontrol.insertarUsuario(nombres, apellidos, nombre_tienda, estado, tipo_usuario, nombre_user, contrasenia);
            message = "{\"message\":\"" + res + "\"}";

        } else {
            message = "{\"status\":3,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/getdatasession")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getDataSession(String data) {
        JsonObject Jso = Methods.stringToJSON(data);
        String user_token = Methods.JsonToString(Jso, "user_token", "");
        String[] res = Methods.getDataToJwt(user_token);
        String jsonresponse = "{\"status\":" + (res[0].equals("") ? 4 : (res[1].equals("sleep")) ? 3 : 2) + ",\"information\":\"" + (res.equals("") ? "Invalid Token." : "All ok.\"")
                + ",\"data\":{\"permmit\":\"" + res[1] + "\"}}";
        return Response.ok(jsonresponse)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarTiendas")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarTiendas(String data) {
        String message;
        System.out.println("listarTiendas()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = ucontrol.listarTiendas();
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/listarClientes")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarClientes(String data) {
        String message;
        System.out.println("listarClientes()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = ucontrol.listarClientes();
                message = res;
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/habilitarUsuario")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response habilitarUsuario(String data) {
        String message;
        System.out.println("habilitarUsuario()");
        System.out.println(data);
        JsonObject Jso = Methods.stringToJSON(data);
        if (Jso.size() > 0) {
            String sessionToken = Methods.JsonToString(Jso, "user_token", "");
            String id_usuario = Methods.JsonToString(Jso, "id_usuario", "");
            System.out.println(sessionToken);
            String[] clains = Methods.getDataToJwt(sessionToken);
            System.out.println(clains[0]);
            if (!clains[0].equals("") && !clains[1].equals("d")) {
                String res = ucontrol.habilitarUsuario(id_usuario);
                message = "{\"message\":\"" + res + "\"}";
                System.out.println(res);
            } else {
                message = "{\"status\":4,\"information\":\"Error in the request parameters.\",\"data\":[]}";
            }
        } else {
            message = "{\"status\":4,\"information\":\"Data are lacking.\",\"data\":{}}";
        }
        return Response.ok(message)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-with")
                .build();
    }

}
