/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.Reader;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author USUARIO
 */
public class decorderMensaje implements Decoder.TextStream<Mensaje>{

    @Override
    public Mensaje decode(Reader reader) throws DecodeException, IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Mensaje msg = new Mensaje();
//        try (JsonReader jreader = Json.createReader(reader)) {
//            JsonObject json = null;
//            json = JsonReader.readObject();
//            msg.setNombre(json.getString("nombre"));
//            msg.setMensaje(json.getString("mensaje"));
//            
//        }
        JsonParser parser = new JsonParser();
        try {
            JsonObject json = (JsonObject) parser.parse(reader);
            msg.setNombre(json.get("nombre").getAsString());
            msg.setMensaje(json.get("mensaje").getAsString());
            
        }catch(Exception e)
        {
            System.out.println("deconding dice:"+e.getMessage());
        }
        return msg;
    }

    @Override
    public void init(EndpointConfig config) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
