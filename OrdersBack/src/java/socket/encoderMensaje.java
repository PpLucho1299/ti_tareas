/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.Writer;
import javax.json.Json;
import javax.json.JsonWriter;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author USUARIO
 */
public class encoderMensaje implements Encoder.TextStream<Mensaje>{

    @Override
    public void encode(Mensaje msg, Writer wrt) throws EncodeException, IOException {

        javax.json.JsonObject json = Json.createObjectBuilder()
             .add("nombre", msg.getNombre())
             .add("mensaje", msg.getMensaje())
                .build();
        try (JsonWriter jsonWriter = Json.createWriter(wrt)){
            jsonWriter.writeObject(json);
        }catch(Exception e)
        {
            System.out.println("enconding dice:"+e.getMessage());
        }
    }

    @Override
    public void init(EndpointConfig config) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
