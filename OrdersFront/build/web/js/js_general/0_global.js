

var GlobalApisLocation = "http://localhost:8080/OrdersBack/webresources/";

function logOff() {
    store.session.set("userorders", undefined);
    location.href = "index.html";
}


function getDataSession() {
    var dataUser = store.session.get("userorders");
    if (dataUser !== undefined && dataUser !== null)
    {
        $.ajax({
            method: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: GlobalApisLocation + 'userApis/getdatasession',
            data: JSON.stringify({"user_token": dataUser.user_token}),
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                swal.close();
                if (data.status === 2)
                {
                    if (dataUser !== undefined)
                    {
                        let nameformat = dataUser.names_user + (dataUser.names_user.toString().indexOf(" ") > -1 ? ("<br/>") : " ") + dataUser.lastname_user;
                        if (document.getElementById("btnNewItems") !== null)
                        {
                            if (dataUser.type_user === "Administrator") {
                                document.getElementById("btnNewItems").style.display = "block";
                            } else {
                                document.getElementById("btnNewItems").style.display = "none";
                            }
                            document.getElementById("nameUser02").innerHTML = nameformat;
                            document.getElementById("imgUser02").src = dataUser.img_user;

                            document.getElementById("names_usermodify").value = dataUser.names_user;
                            document.getElementById("lastnames_usermodify").value = dataUser.lastname_user;
                            document.getElementById("email_usermodify").value = dataUser.email_user;
                            document.getElementById("phone_usermodify").value = dataUser.phonenumbre_user;
                            document.getElementById("urlimg_usermodify").value = (dataUser.img_user === undefined) ? "" : dataUser.img_user;
                        }
                        if (document.getElementById("nameUser01") !== null)
                        {
                            document.getElementById("nameUser01").innerHTML = dataUser.names_user + " " + dataUser.lastname_user;
                            document.getElementById("rolUser").innerHTML = dataUser.type_user;
                        }
                        document.getElementById("imgUser01").src = dataUser.img_user;
                    } else {
                        allMessageXD({status: 4, information: "The session was destroyed :c."});
                    }
                } else if (data.status === 5)
                {
                    location.href = "notverified.html";
                } else
                {
                    location.href = "login.html";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                location.href = "login.html";
            }
        });

    } else
    {
        location.href = "login.html";
    }
}

