/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app = angular.module('app', []);
app.controller('controllerSend', function ($scope, $http) {

    $scope.dataSend = [];
    $scope.permit = false;
    $scope.datasession = "";
    $scope.totales = {};
    $scope.detalle = [];
    $scope.detailSendOnly = [];
    $scope.valorTotalDetail = {
        "subtotal": 0,
        "iva": 0,
        "totalapagar": 0
    };
    var subtotal = 0;
    var iva = 0;
    var totalapagar = 0;
    $(document).ready(function () {
        //cargar los datos de la sesion
        getDatosSession();
        $("#listadoenvio").show();
        $("#nuevoenvio").hide();
        $scope.getTableSend();
        getPermit();
    });

    $scope.getTableSend = function () {
        $.ajax({
            type: "POST",
            url: 'ServletSend',
            data: {"opcion": "selectSend"},
            beforeSend: function () {
                cargando();
            },
            success: function (data) {
                swal.close();
                $scope.$apply(function () {
                    $scope.dataSend = JSON.parse(data);
                    console.log($scope.dataSend);
                });
            },
            error: function (objXMLHttpRequest) {
                console.log("error", objXMLHttpRequest);
                swal.fire("!Oh no¡", "Se ha producido un problema.", "error");
            }
        });
    };

    $scope.getDetailSend = function (id_send, position) {
        $.ajax({
            type: "POST",
            url: 'ServletSend',
            data: {"opcion": "selectDetail", "id_send": id_send},
            beforeSend: function () {
                cargando();
            },
            success: function (data) {
                swal.close();
                $("#detailSendModal").modal()
                $scope.$apply(function () {
                    $scope.detailSendOnly = JSON.parse(data);
                    console.log($scope.detailSendOnly);
                    $scope.valorTotalDetail.subtotal = 0;
                    $("#clienteview").html($scope.dataSend[position].apellidos +' '+$scope.dataSend[position].nombres);
                    $("#direccionview").html($scope.dataSend[position].direccion_envio);
                    $("#emailview").html($scope.dataSend[position].email);
                    $("#fechaenvioview").html($scope.dataSend[position].fecha_envio);
                    for (var i = 0; i < $scope.detailSendOnly.length; i++) {
                        let val0 = parseFloat($scope.detailSendOnly[i].val0);
                        let val1 = parseFloat($scope.detailSendOnly[i].val1);
                        let val2 = parseFloat($scope.detailSendOnly[i].val2);
                        let cantidad = parseFloat($scope.detailSendOnly[i].cantidad);
                        let precio = parseFloat($scope.detailSendOnly[i].precio);
                        $scope.valorTotalDetail.subtotal += val0 + val1 + val2 + (cantidad * precio);
                        //console.log($scope.valorTotalDetail.subtotal);
                    }
                    $scope.valorTotalDetail.iva = $scope.valorTotalDetail.subtotal * 0.12;
                    $scope.valorTotalDetail.totalapagar = $scope.valorTotalDetail.subtotal + $scope.valorTotalDetail.iva;
                    console.log($scope.valorTotalDetail);
                });
            },
            error: function (objXMLHttpRequest) {
                console.log("error", objXMLHttpRequest);
                swal.fire("!Oh no¡", "Se ha producido un problema.", "error");
            }
        });
    }

    $("#btn-realizarenvio").click(function () {

        if ($("#ip_direccionevio").val().trim() === "") {
            alert("Ingresa la direccion de envio.");
            return;
        }

        if ($scope.detalle.length <= 0) {
            alert("Ingresa al menos un producto");
            return;
        }

        let send = {
            "direccion": $("#ip_direccionevio").val(),
            "detalle": getDetalle()
        };
        insertSend(send);
    });
    //cerrar sesion
    $("#btn-cerrar-sesion").click(function () {
        cerrarSesion();
    });
    $("#btn-nuevo-envio").click(function () {
        $("#listadoenvio").hide();
        $("#nuevoenvio").show();
    });
    $("#btn-cancelar-envio").click(function () {
        reiniciarEnvio();
        $("#listadoenvio").show();
        $("#nuevoenvio").hide();
    });
    function insertSend(jsonData) {
        $.ajax({
            type: "POST",
            url: 'ServletSend',
            data: {"opcion": "insertSend", ...jsonData},
            beforeSend: function () {
                cargando();
            },
            success: function (data) {
                swal.close();
                console.log(data);
                alert(data);
                reiniciarEnvio();
                $("#listadoenvio").show();
                $("#nuevoenvio").hide();
                $scope.getTableSend();
            },
            error: function (objXMLHttpRequest) {
                console.log("error", objXMLHttpRequest);
                swal.fire("!Oh no¡", "Se ha producido un problema.", "error");
            }
        });
    }

    function getDetalle() {
        let detalle = "";
        for (let i = 0; i < $scope.detalle.length; i++) {
            detalle += $scope.detalle[i].producto + ";" + $scope.detalle[i].cantidad + ";" + $scope.detalle[i].precio + ";" + $scope.detalle[i].tamanio
                    + ";" + $scope.detalle[i].peso + ";" + $scope.detalle[i].tipoproducto;
            if (i < $scope.detalle.length - 1) {
                detalle += "/";
            }
        }
        console.log(detalle);
        return detalle;
    }

    function getPermit() {
        let rdata;
        $.ajax({
            type: "POST",
            url: 'ServletSession',
            data: {"opcion": "ses"},
            beforeSend: function () {
                cargando();
            },
            success: function (data) {
                swal.close();
                rdata = JSON.parse(data);
                console.log(rdata);
                $scope.datasession = rdata.userObject.getCargo
                console.log($scope.datasession);
            },
            error: function (objXMLHttpRequest) {
                console.log("error", objXMLHttpRequest);
                swal.fire("!Oh no¡", "Se ha producido un problema.", "error");
            }
        });
    }

    $scope.addProduct = function () {

        if ($("#ip_producto").val().trim() === "") {
            alert("Ingrese el nombre del producto");
            return;
        }

        if ($("#ip_cantidad").val().trim() === "") {
            alert("Ingrese la cantidad del producto");
            return;
        }

        if ($("#ip_precio").val().trim() === "") {
            alert("Ingrese el precio del producto");
            return;
        }

        if (parseInt($("#ip_cantidad").val().trim()) < 0) {
            alert("Ingrese valores validos");
            return;
        }

        if (parseFloat($("#ip_precio").val().trim()) < 0) {
            alert("Ingrese valores validos");
            return;
        }


        $scope.detalle.push({
            "producto": $("#ip_producto").val(),
            "cantidad": $("#ip_cantidad").val(),
            "precio": $("#ip_precio").val(),
            "tamanio": $("#ip_tamanio").val(),
            "peso": $("#ip_peso").val(),
            "tipoproducto": $("#ip_tipoproducto").val()
        });
        console.log($scope.detalle);
        calcularTotalAPagar({
            "cantidad": $("#ip_cantidad").val(),
            "precio": $("#ip_precio").val(),
            "tamanio": $("#ip_tamanio").val(),
            "peso": $("#ip_peso").val(),
            "tipoproducto": $("#ip_tipoproducto").val()
        }, "add");
        $("#ip_producto").val("");
        $("#ip_cantidad").val("");
        $("#ip_precio").val("");
    };
    $scope.deleteProduct = function (index) {
        calcularTotalAPagar({
            "cantidad": $scope.detalle[index].cantidad,
            "precio": $scope.detalle[index].precio,
            "tamanio": $scope.detalle[index].tamanio,
            "peso": $scope.detalle[index].peso,
            "tipoproducto": $scope.detalle[index].tipoproducto
        }, "del");
        $scope.detalle.splice(index, 1);
    };
    function calcularTotalAPagar(jsonData, accion) {
        let valorTamanio = jsonData.tamanio === "pequenio" ? 2 : jsonData.tamanio === "mediano" ? 3.50 : 5;
        let valorPeso = jsonData.peso === "liviano" ? 2.50 : jsonData.tamanio === "pesado" ? 4.50 : 8;
        let valorTipProd = jsonData.tipoproducto === "comida" ? 3 : jsonData.tamanio === "inmueble" ? 10 : 7.50;
        console.log(valorTamanio + ' ' + valorPeso + ' ' + valorTipProd);
        let precioprodcuto = parseFloat(jsonData.precio);
        let cantidad = parseFloat(jsonData.cantidad);
        if (accion === "add") {
            subtotal += subTotal({tamanio: valorTamanio, peso: valorPeso, tpro: valorTipProd, pc: precioprodcuto * cantidad});
            iva = calcularIva(subtotal);
            totalapagar = subtotal + iva;
        } else {
            subtotal -= subTotal({tamanio: valorTamanio, peso: valorPeso, tpro: valorTipProd, pc: precioprodcuto * cantidad});
            iva = calcularIva(subtotal);
            totalapagar = subtotal + iva;
        }

        console.log(subtotal + ' ' + iva + ' ' + totalapagar);
        $scope.totales = {
            "subtotal": "$ " + subtotal.toFixed(2),
            "iva": "$ " + iva.toFixed(2),
            "totalapagar": "$ " + totalapagar.toFixed(2)
        };
    }

    function subTotal(jsonValores) {
        return jsonValores.tamanio + jsonValores.peso + jsonValores.tpro + jsonValores.pc;
    }

    function calcularIva(subtotal) {
        return subtotal * 0.12;
    }

    function reiniciarEnvio() {
        $scope.$apply(function () {
            $scope.detalle.length = 0;
            subtotal = 0;
            iva = 0;
            totalapagar = 0;
            $scope.totales = {
                "subtotal": "$ " + subtotal.toFixed(2),
                "iva": "$ " + iva.toFixed(2),
                "totalapagar": "$ " + totalapagar.toFixed(2)
            };
        });
        //limpiar form de envio
        $("#ip_direccionevio").val("");
        $("#ip_producto").val("");
        $("#ip_cantidad").val("");
        $("#ip_precio").val("");
    }

    function validaNumericos(id_input) {
        var inputtxt = document.getElementById(id_input);
        var valor = inputtxt.value;
        for (i = 0; i < valor.length; i++) {
            var code = valor.charCodeAt(i);
            if (code <= 48 || code >= 57) {
                inputtxt.value = "";
                return false;
            } else {
                return true;
            }
        }

    }

});


